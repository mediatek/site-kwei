from django.urls import include, path

from accounts import views

app_name = "accounts"

urlpatterns = [
	path("update/", views.UpdateAccountView.as_view(), name="update"),
]
